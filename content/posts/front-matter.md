---
title: "Keep-fit Programmer"
date: 2017-09-28T20:49:57+02:00
draft: false
---

# Some thoughts about a "Keep-fit Programmer" 
I'm think about a self-powered, low energy consumption computer specifically designed for hard-core programmers.

# Features
+ Workout bicycle
+ Dynamo power generator
+ Battery-powered, low energy consumption
    - Command-line
    - Low-resolution screen
    - Mechanical keyboard
    - Minimum number of wireless modules


